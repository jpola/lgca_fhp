#ifndef _FHP_GRID_HPP_
#define _FHP_GRID_HPP_

#include <vector>
#include "Node.hpp"
#include <boost/random.hpp>

class Grid
{
    using CELLS = std::vector<Node>;
    using BOOLEAN_BITS = std::vector<bool>;


public:

    Grid() : _x_size(0), _y_size(0),
        _cells(0), _random_bits(0),// _obstacles(0),
        collision_table_above(0), collision_table_below(0)
    {
        //        rnd.seed(time(0));
        //        generate_random_bits();
    }

    explicit Grid(int x, int y) :
        _x_size(x), _y_size(y),
        _cells(_x_size*_y_size), _random_bits(_x_size * _y_size),
        //_obstacles(_x_size*_y_size),
        collision_table_above(64), collision_table_below(64)
    {
        rnd.seed(time(0));
        generate_random_bits();
        generate_collision_table();
        //generate_channel(2);
    }

    int get_x_size() const
    {
        return _x_size;
    }

    int get_y_size() const
    {
        return _y_size;
    }

    int get_size() const
    {
        return _cells.size();
    }

    Node& get_cell(int x, int y)
    {
        //fast modulo if x is 1 << n (potega liczby 2);
        x = x & (_x_size - 1 );
        y = y & (_y_size - 1 );
        //x = (x + _x_size) % _x_size;
        //y = (y + _y_size) % _y_size;
        //std::cout << "x = " << x << " y = " << y << std::endl;
        return _cells[x + _x_size * y];

    }

    bool get_random_bit(int x, int y)
    {
        x = x & (_x_size - 1 );
        y = y & (_y_size - 1 );

        return _random_bits[x + _x_size * y];

    }


    Node& get_cell(int i)
    {
        assert (i < _cells.size());
        return _cells[i];

    }

    const Node& get_cell(int x, int y) const
    {
        //fast modulo
        x = x & (_x_size - 1 );
        y = y & (_y_size - 1 );

        //x = (x + _x_size) % _x_size;
        //y = (y + _y_size) % _y_size;
        //std::cout << "x = " << x << " y = " << y << std::endl;
        return _cells[x + _x_size * y];

    }

//    bool get_obstacle(int x, int y)
//    {
//        //fast modulo
//        x = x & (_x_size - 1 );
//        y = y & (_y_size - 1 );

//        //x = (x + _x_size) % _x_size;
//        //y = (y + _y_size) % _y_size;
//        //std::cout << "x = " << x << " y = " << y << std::endl;
//        return _obstacles[x + _x_size * y];

//    }

    void generate_random_bits()
    {
        boost::uniform_int<> zero_one(0, 1);
        boost::variate_generator< boost::random::mt19937, boost::uniform_int<> >
                dice(rnd, zero_one);

        std::generate(_random_bits.begin(), _random_bits.end(), dice);

    }

    unsigned char get_collision_result(const Node& state, int x, int y)
    {
        if (get_random_bit(x, y))
            return collision_table_above[state.get_state()];
        else
            return collision_table_below[state.get_state()];
    }

//    void generate_channel(int thickness)
//    {
//        std::fill(_obstacles.begin(), _obstacles.end(), false);
//        for (int y = 0; y < thickness; y++ )
//            for(int x = 0; x < _x_size ; x++)
//            {
//                {
//                    int i = x & (_x_size - 1 );
//                    int j = y & (_y_size - 1 );
//                    _obstacles[i + _x_size * j] = true;
//                }
//            }

//        for (int y = _y_size - thickness; y < _y_size; y++ )
//            for(int x = 0; x < _x_size ; x++)
//            {
//                {
//                    int i = x & (_x_size - 1 );
//                    int j = y & (_y_size - 1 );
//                    _obstacles[i + _x_size * j] = true;
//                }
//            }

//    }

private:

    void generate_collision_table()
    {
        unsigned char count = 0;
        std::generate(collision_table_above.begin(),
                      collision_table_above.end(), [&](){ return count++; });

        std::copy(collision_table_above.begin(),
                  collision_table_above.end(), collision_table_below.begin());

        //binary collisions;
        //clockwise rotation
        collision_table_below[9] = 18;
        collision_table_below[18] = 36;
        collision_table_below[36] = 9;
        //anti-clockwise rotation
        collision_table_above[9] = 36;
        collision_table_above[36] = 18;
        collision_table_above[18] = 9;

        //tri-particle collisons;
        collision_table_above[21] = 42;
        collision_table_above[42] = 21;

        collision_table_below[21] = 42;
        collision_table_below[42] = 21;

        std::cout << std::endl;

    }



    int _x_size;
    int _y_size;

    CELLS _cells;
    BOOLEAN_BITS _random_bits;
    //BOOLEAN_BITS _obstacles;

    //two versions of collision tables;
    //depends on the _random_bits it will be one or another;
    std::vector <unsigned char> collision_table_below;
    std::vector <unsigned char> collision_table_above;

    boost::random::mt19937 rnd;

};


#endif //_FHP_GRID_HPP_
