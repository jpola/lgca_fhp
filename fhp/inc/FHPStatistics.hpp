#ifndef _FHP_STATISTICS_FHP_
#define _FHP_STATISTICS_FHP_

#include "Grid.hpp"


std::vector<double> Ci = {0.5 + 0.866025, -0.5 + 0.866025,
                        -1, -0.5 - 0.866025, 0.5 - 0.866025, 1};


double check_momentum_conservation(Grid& grid)
{
    int x_size = grid.get_x_size();
    int y_size = grid.get_y_size();

    int size = x_size * y_size;

    double momentum = 0.0;

    for (int j = 0; j < y_size; j++)
    {
        for (int i = 0; i < x_size; i++)
        {
            auto cell_bitset = grid.get_cell(i, j).get_bits();

            for (int i = 0; i < 6; i++)
            {
                momentum += Ci[i] * cell_bitset[i];
            }
        }
    }

    return momentum;

}

int check_mass_conservation(Grid& grid)
{
    int x_size = grid.get_x_size();
    int y_size = grid.get_y_size();

    int size = x_size * y_size;

    //std::vector<int> mass_elements(size);

    int mass = 0;

    for (int j = 0; j < y_size; j++)
    {
        for (int i = 0; i < x_size; i++)
        {
            mass += grid.get_cell(i, j).get_bits().count();
        }
    }

//    std::cout << "mass = " << mass << std::endl;
    return mass;
}

double check_grid_node_density(Grid& grid)
{
    int x_size = grid.get_x_size();
    int y_size = grid.get_y_size();

    int size = x_size * y_size;

    //std::vector<int> mass_elements(size);

    double density = 0;

    for (int j = 0; j < y_size; j++)
    {
        for (int i = 0; i < x_size; i++)
        {
            density += grid.get_cell(i, j).get_bits().count() / 6.0;
        }
    }
    return density / size;
}

#endif //_FHP_STATISTICS_FHP_
