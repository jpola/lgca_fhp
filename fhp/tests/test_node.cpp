#include <gtest/gtest.h>
#include <Node.hpp>


TEST (FHP_NODE, print)
{
    Node n(255);
    std::cout << n << std::endl;


    auto bits = n.get_bits();
    for (int i = 0; i < 8; i++)
    {
        ASSERT_EQ(bits[i], 1);
    }

}

TEST (FHP_NODE, test_bit)
{
    Node n0(0);
    for (int i = 0; i < n0.size(); i++)
    {
        EXPECT_EQ(0, n0.check_bit(i));
    }

    Node n1(255);
    for (int i = 0; i < n1.size(); i++)
    {
        EXPECT_EQ(1, n1.check_bit(i));
    }

    Node n2(1);
    EXPECT_EQ(1, n2.check_bit(0));
    for (int i = 1; i < n2.size(); i++)
    {
        EXPECT_EQ(0, n2.check_bit(i));
    }

}

TEST (FHP_NODE, overflow)
{
    ASSERT_DEATH(Node(257), "state <= MAX_STATE && state >= 0");
}

TEST (FHP_NODE, set_bit)
{

    //sanity check
    Node n1(0);
    for (int i = 0; i < n1.size(); i++)
    {
        EXPECT_EQ(0, n1.check_bit(i));
    }

    //set the lowest bit;
    n1.set_bit(0);
    EXPECT_EQ(1, n1.check_bit(0));

    //set the second bit
    n1.set_bit(1);
    EXPECT_EQ(1, n1.check_bit(1));

    //set_bit sets to 1 not flip!
    n1.set_bit(1);
    EXPECT_EQ(1, n1.check_bit(1));

    //you are not allowed to set position different than (0, 7);
    ASSERT_DEATH(n1.set_bit(-1), "pos < _size && pos >= 0");
    ASSERT_DEATH(n1.set_bit(8), "pos < _size && pos >= 0");


}

TEST (FHP_NODE, clear_bit)
{
    //sanity check
    Node n1(255);
    for (int i = 0; i < n1.size(); i++)
    {
        EXPECT_EQ(1, n1.check_bit(i));
    }

    //set the lowest bit;
    n1.unset_bit(0);
    EXPECT_EQ(0, n1.check_bit(0));

    //set the second bit
    n1.unset_bit(1);
    EXPECT_EQ(0, n1.check_bit(1));

    //set_bit sets to 1 not flip!
    n1.unset_bit(0);
    EXPECT_EQ(0, n1.check_bit(1));

    //you are not allowed to set position different than (0, 7);
    ASSERT_DEATH(n1.unset_bit(-1), "pos < _size && pos >= 0");
    ASSERT_DEATH(n1.unset_bit(8), "pos < _size && pos >= 0");

}

TEST (FHP_NODE, set_value)
{
    Node n(0);

    n.set_value(0, 1);

    EXPECT_EQ(1, n.check_bit(0));

    n.set_value(0, 0);

    EXPECT_EQ(0, n.check_bit(0));

    n.set_value(1, 0);
    EXPECT_EQ(0, n.check_bit(1));

}

TEST(FHP_NODE, check_bit)
{
    Node n(63);

    std::cout << n << std::endl;

    for (int i = 0; i < 6; i++)
        EXPECT_EQ(true, n.check_bit(i));
}

TEST (FHP_NODE, bitset)
{
    Node n(63);

    auto bset = n.get_bits();

    std::cout << bset.count() << std::endl;

    std::cout << bset << std::endl;
    std::cout << bset[5] << std::endl;
}

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();

}
