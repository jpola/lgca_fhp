#include <iostream>
#include <cmath>
#include <algorithm>


#include "CoarseGrid.hpp"
#include "GridUtils.hpp"
#include "Renderer.hpp"


Renderer* Renderer::instance = NULL;

int main(int argc, char* argv[])
{

/* Simple calculations for Re = 90
 *
 * cs = 0.8760
 * u = 0.2
 * d = 2/6 = 1/3
 * ni = 1/12 * 1/d*(1-d)^3 - 1/8 = 0.71875
 * L ~= Re*ni/u = 323
 *
 *
 */

    int N = 1 << 7;//10;
    int M = 1 << 5;//7;

    int ratio = N / M;
    int cM = M / 8;   //tile y size
    int cN = cM*ratio;  //tile x size
    CoarseGrid grid(N, M, cN, cM);
    std::cout << "FHP, grid size: (" << N << ", " << M << ")" << std::endl;
    std::cout << "Coarse grid size (" << cN << ", " << cM << ")" << std::endl;

    clear_grid(grid);

    randomize_grid(grid, 1);

    //make_hole(grid, N/2, M/2, N/4);
    //make_bump(grid, N/2, N/3, 16, 63);
    make_channel(grid, 1);

    //put_obstacle(grid, N/4, M/2, 16);

    Renderer renderer(grid);
    renderer.init(argc, argv);
    renderer.run();


	return 0;
}
