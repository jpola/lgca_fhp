#ifndef _FHP_FHPDYNAMICS_HPP_
#define _FHP_FHPDYNAMICS_HPP_

#include "CoarseGrid.hpp"


void Propagate (Grid& grid)
{
    Grid grid_tmp = grid; //clone grid
#ifdef NDEBUG
    #pragma omp parallel for
#endif
    for (int y = 0; y < grid.get_y_size(); y++)
    {
        for (int x = 0; x < grid.get_x_size(); x++)
        {
            auto& c = grid.get_cell(x, y);
            /** cell organisation
             *  2   1
             * 3  6  0
             *  4   5
             */
           //destination cells
            auto& n0 = grid_tmp.get_cell(x + 1, y);
            auto& n1 = grid_tmp.get_cell(x, y + 1);
            auto& n2 = grid_tmp.get_cell(x - 1, y + 1);
            auto& n3 = grid_tmp.get_cell(x - 1, y);
            auto& n4 = grid_tmp.get_cell(x, y - 1);
            auto& n5 = grid_tmp.get_cell(x + 1, y - 1);

            n0.set_value(0, c.check_bit(0)); //c.unset_bit(0);
            n1.set_value(1, c.check_bit(1)); //c.unset_bit(1);
            n2.set_value(2, c.check_bit(2)); //c.unset_bit(2);
            n3.set_value(3, c.check_bit(3)); //c.unset_bit(3);
            n4.set_value(4, c.check_bit(4)); //c.unset_bit(4);
            n5.set_value(5, c.check_bit(5)); //c.unset_bit(5);
        }
    }
    std::swap(grid, grid_tmp);
}

void Collide (Grid& grid)
{
    Grid grid_tmp = grid; //clone the grid;

#ifdef NDEBUG
    #pragma omp parallel for
#endif
    for (int y = 0; y < grid.get_y_size(); y++)
    {
        for (int x = 0; x < grid.get_x_size(); x++)
        {
            //current bits state of the cell
            auto& cell_bitset = grid.get_cell(x, y).get_bits();
            //std::cout << "\nbefore collision = " << cell_bitset << std::endl;
            //new bits state of the cell (this will be changed)
            auto& cell_new_value = grid_tmp.get_cell(x, y);
            auto cell_new_bitset_state = cell_new_value.get_bits();

            bool a = cell_bitset[0];
            bool b = cell_bitset[1];
            bool c = cell_bitset[2];
            bool d = cell_bitset[3];
            bool e = cell_bitset[4];
            bool f = cell_bitset[5];

            // two body colision;
            bool db1 = (a & d & ~(b | c | e | f));
            bool db2 = (b & e & ~(a | c | d | f));
            bool db3 = (c & f & ~(a | b | d | e));

            //triple body colision
            bool tripple = (a ^ b) & (b ^ c) & (c ^ d) & (d ^ e) & (e ^ f);

            bool xi = grid.get_random_bit(x, y); //irn[x][y];       /* random bits */

            //perform colision only on fluid nodes
            bool nsbit = ~cell_bitset[7];

            //status of change
            bool cha = ((tripple | db1 | (xi & db2) | (~xi & db3)) & nsbit);
            bool chd = ((tripple | db1 | (xi & db2) | (~xi & db3)) & nsbit);
            bool chb = ((tripple | db2 | (xi & db3) | (~xi & db1)) & nsbit);
            bool che = ((tripple | db2 | (xi & db3) | (~xi & db1)) & nsbit);
            bool chc = ((tripple | db3 | (xi & db1) | (~xi & db2)) & nsbit);
            bool chf = ((tripple | db3 | (xi & db1) | (~xi & db2)) & nsbit);


            cell_new_bitset_state.set(0, a ^ cha);
            cell_new_bitset_state.set(1, b ^ chb);
            cell_new_bitset_state.set(2, c ^ chc);
            cell_new_bitset_state.set(3, d ^ chd);
            cell_new_bitset_state.set(4, e ^ che);
            cell_new_bitset_state.set(5, f ^ chf);
            cell_new_bitset_state.set(7, cell_bitset[7]);

            auto i = cell_bitset.to_ulong();

            i = cell_new_bitset_state.to_ulong(); //& 63;

            cell_new_value = static_cast<unsigned char>( i );

        }
    }

    std::swap(grid, grid_tmp);
    grid.generate_random_bits();

}

void BounceBack(Grid& grid)
{
    Grid grid_tmp = grid; //clone the grid;

#ifdef NDEBUG
    #pragma omp parallel for
#endif
    for (int y = 0; y < grid.get_y_size(); y++)
    {
        for (int x = 0; x < grid.get_x_size(); x++)
        {
            //current bits state of the cell
            auto& cell = grid.get_cell(x, y);
            auto cell_bitset = cell.get_bits();

            auto& cell_new_value = grid_tmp.get_cell(x, y);
            auto  cell_new_bitset_state = cell_new_value.get_bits();

            if (cell_bitset[7]) //if obstacle
            {
                cell_new_bitset_state.set(0, cell_bitset[3]);
                cell_new_bitset_state.set(1, cell_bitset[4]);
                cell_new_bitset_state.set(2, cell_bitset[5]);
                cell_new_bitset_state.set(3, cell_bitset[0]);
                cell_new_bitset_state.set(4, cell_bitset[1]);
                cell_new_bitset_state.set(5, cell_bitset[2]);

                auto i = cell_new_bitset_state.to_ulong();

                cell_new_value = static_cast<unsigned char> (cell_new_bitset_state.to_ulong());

            }
        }
    }

    std::swap(grid, grid_tmp);
}


void Collide_by_lookup_table(Grid& grid)
{
    Grid grid_tmp = grid; //clone the grid;

//#pragma omp parallel for
    for (int y = 0; y < grid.get_y_size(); y++)
    {
        for (int x = 0; x < grid.get_x_size(); x++)
        {
            auto current_cell_state = grid.get_cell(x, y);

            auto& cell_new_value = grid_tmp.get_cell(x, y);

            cell_new_value = grid.get_collision_result(current_cell_state, x, y);

        }
    }

    std::swap(grid, grid_tmp);
    grid.generate_random_bits();

}

void ImposeFlow(CoarseGrid& grid)
{
    grid.set_vertitical_density(0, 2.2);
    grid.set_vertitical_density(grid.n_tails_x()-1, 0.5);

}



#endif //_FHP_FHPDYNAMICS_HPP_
