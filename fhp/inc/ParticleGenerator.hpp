#ifndef _PARTICLE_GENERATOR_HPP_
#define _PARTICLE_GENERATOR_HPP_

#include <boost/random.hpp>


class ParticleGenerator
{
public:
    ParticleGenerator(double density, double sigma) :
        rng(boost::random::mt19937(time(0))),
        distribution(density, 1),
        number_of_particles(rng, distribution)
    {

    }

    int get_n_particles()
    {
        return number_of_particles();
    }

private:
    boost::random::mt19937 rng;
    boost::normal_distribution<> distribution;//(density,1);
    boost::variate_generator< boost::random::mt19937, boost::normal_distribution<>>
                number_of_particles;

};



#endif //_PARTICLE_GENERATOR_HPP_
