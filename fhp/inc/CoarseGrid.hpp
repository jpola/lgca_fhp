#ifndef _COARSE_GRID_HPP_
#define _COARSE_GRID_HPP_


#include <vector>
#include "Grid.hpp"
#include "ParticleGenerator.hpp"

// to get range of grid with given tile x,y coords;
// x: tx * nx + tile_x_size;
// to get


/* TODO:
 * - add grid reference field
 * - add get density from axis
 * - add set density from axis
 *
 * - does the reference must be updated after each timestep where they are swapped!
 *      probably NOT!
 */


class CoarseGrid : public Grid
{
public:

    //there is no time to play with const correctnes. but here should be const CoarseGrid&
    //and I should wirite proper const funct () const {} functions for CoarseGrid class to
    //manipulate CoarseGrid from CoarseGrid

    //x - x size of the grid
    //y - y size of the grid
    //tx - x size of the coarse tile
    //ty - y size of the coarse tile
    CoarseGrid(int x, int y, int tx, int ty) :
        Grid(x, y),
        tile_x_size(tx), tile_y_size(ty),
        nx(x / tile_x_size), ny(y / tile_y_size), size(nx*ny),
        density(size), momentum(size),
        Cx(6), Cy(6), C(6)
    {
        for(int i = 0; i < 6; i++)
        {
            Cx[i] = std::cos((i+1) * M_PI/3.);
            Cy[i] = std::sin((i+1) * M_PI/3.);
            C[i] = std::sqrt(Cx[i]*Cx[i] + Cy[i]*Cy[i]);
            //std::cout << "Cx = " << Cx[i] << "\tCy = " << Cy[i] << "\tC = " << C[i] <<   std::endl;
        }

    }


    //density is just number of particles in tile;
    void calculate_density()
    {
        std::fill(density.begin(), density.end(), 0);

        for (int x = 0; x < get_x_size(); x++)
        {
            for (int y = 0; y < get_y_size(); y++)
            {
                int tile_x_index = x / tile_x_size;
                int tile_y_index = y / tile_y_size;

                int tile_index = tile_x_index + nx * tile_y_index;

                assert(tile_index < density.size());

                density[tile_index] += get_cell(x, y).get_bits().count();
            }
        }
    }

    double calculate_avg_density()
    {
        calculate_density();

        return system_density() / get_size();
    }

    double system_density()
    {
        return std::accumulate(density.begin(), density.end(), 0.0);
    }


    std::vector<double> get_velocity_mag(std::vector<double>& vx,
                                         std::vector<double>& vy,
                                         std::vector<double>& vm)
    {

        vm.resize(nx*ny, 0.0);
        vx = vy = vm;

        for(int y = 0; y < ny; y++)
        {
            for(int x = 0; x < nx; x++)
            {
                //range of cells in tile (x, y);
                int yp = y * tile_y_size; //index of first cell in y direction in tile coord y
                int ye = yp + tile_y_size;//index of last cell in y direction in tile coord y

                int xp = x * tile_x_size;
                int xe = xp + tile_x_size;

                //sum values in particular tail;
                for (int j = yp; j < ye; j++)
                {
                    for (int i = xp; i < xe; i++)
                    {
                        auto bits = get_cell(i, j).get_bits();

                        double _vx = 0;
                        double _vy = 0;
                        for (int b = 0; b < 6; b++)
                        {
                            int index = x + nx * y;
                            _vx += bits[b] * Cx[b];
                            _vy += bits[b] * Cy[b];
                            vx[index] += _vx;
                            vy[index] += _vy;
                            vm[index] += sqrt(_vx*_vx + _vy*_vy);
                        }
                    }
                }
            }
        }

        return vm;
    }

    std::vector<double> get_vertical_vy(int x)
    {
        std::vector<double> vy(ny, 0.0);

        for(int y = 0; y < ny; y++)
        {

            int yp = y * tile_y_size;
            int ye = yp + tile_y_size;

            int xp = x * tile_x_size;
            int xe = xp + tile_x_size;

            for (int j = yp; j < ye; j++)
            {
                for (int i = xp; i < xe; i++)
                {

                    auto bits = get_cell(i, j).get_bits();

                    for (int b = 0; b < 6; b++)
                    {
                        vy [y] += bits[b] * Cy[b];
                    }
                }
            }

            vy[y] /= double(tile_y_size*tile_x_size);
        }
        return vy;
    }

    std::vector<double> get_vertical_vx(int x)
    {
        std::vector<double> vx(ny, 0.0);

        for(int y = 0; y < ny; y++)
        {

            int yp = y * tile_y_size;
            int ye = yp + tile_y_size;

            int xp = x * tile_x_size;
            int xe = xp + tile_x_size;

            for (int j = yp; j < ye; j++)
            {
                for (int i = xp; i < xe; i++)
                {

                    auto bits = get_cell(i, j).get_bits();

                    for (int b = 0; b < 6; b++)
                    {
                        vx [y] += bits[b] * Cx[b];
                    }
                }
            }
            //average per cell in tile square;
            vx[y] /= double(tile_y_size*tile_x_size);
        }
        return vx;
    }

    std::vector<double> get_vertical_density (int x)
    {
        std::vector<double> d(ny, 0);

        for(int y = 0; y < ny; y++)
        {

            int yp = y * tile_y_size;
            int ye = yp + tile_y_size;

            int xp = x * tile_x_size;
            int xe = xp + tile_x_size;



            for (int j = yp; j < ye; j++)
            {
                for (int i = xp; i < xe; i++)
                {

                    d[y] += get_cell(i, j).get_bits().count();
                }
            }

            d[y] /= double(tile_x_size*tile_y_size*6);
        }
        return d;

    }

    void set_vertitical_density(int x, double density)
    {
        ParticleGenerator p_generator (density, 1.0);
        std::random_device rd;
        std::mt19937 shuffler(rd());

        for(int y = 0; y < ny; y++)
        {

            int yp = y * tile_y_size;
            int ye = yp + tile_y_size;

            int xp = x * tile_x_size;
            int xe = xp + tile_x_size;

            for (int j = yp; j < ye; j++)
            {
                for (int i = xp; i < xe; i++)
                {
                    auto& cell = get_cell(i, j);
                    cell = generate_cell_state(p_generator.get_n_particles(),
                                               shuffler).to_ulong();

                }
            }
        }

    }




    int n_tails_x()
    {
        return nx;
    }

    int n_tails_y()
    {
        return ny;
    }

    int get_x_tile_size()
    {
        return tile_x_size;
    }

    int get_y_tile_size()
    {
        return tile_y_size;
    }


private:

    std::bitset<8> generate_cell_state(int number_of_particles,
                                       std::mt19937& shuffler)
    {
        std::bitset<8> cell_state(0);
        //for shuffling;
        std::vector<bool> bits = {false, false, false, false, false, false};

        //set many particles should be in current node
        // get the number from normal distribution rounded by integer type
        for (int i = 0; i < number_of_particles; i++)
        {
            bits[i] = true;
        }

        std::shuffle(bits.begin(), bits.end(), shuffler);

        for (int i = 0; i < bits.size(); i++)
        {
            cell_state[i] = bits[bits.size() - i - 1];
        }

        return cell_state;


    }

    //size of the tile in x, y direction;
    int tile_x_size;
    int tile_y_size;

    //number of tiles in x, y direction;
    int nx;
    int ny;

    int size;

    //each element of the vector keep information about mass and momentum in the tile.
    //tiles are enumerated as [x + x_size * y]
    std::vector<double> density;
    std::vector<double> momentum;

    std::vector<double> vx;
    std::vector<double> vy;

    //values of ci
    std::vector<double> Cx;
    std::vector<double> Cy;
    std::vector<double> C;


};

#endif //_COARSE_GRID_HPP_
