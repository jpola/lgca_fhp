#ifndef _FHP_GRID_UTILS_HPP_
#define _FHP_GRID_UTILS_HPP_

#include "ParticleGenerator.hpp"
#include "CoarseGrid.hpp"

void randomize_grid(Grid& grid, int density = 5)
{
    //shuffle randomizer
    std::random_device rd;
    std::mt19937 shuffler(rd());

    //normal distribution randomizer
    ParticleGenerator p_generator (density, 1);

    int x = grid.get_x_size();
    int y = grid.get_y_size();

    for(int j = 0; j < y; j++)
    {
        for(int i = 0; i < x; i++)
        {

            std::bitset<8> cell_state(0);
            //for shuffling;
            std::vector<bool> bits = {false, false, false, false, false, false};

            //set many particles should be in current node
            // get the number from normal distribution rounded by integer type
            int np = p_generator.get_n_particles();
            for (int i = 0; i < np; i++)
            {
                bits[i] = true;
            }

            std::shuffle(bits.begin(), bits.end(), shuffler);

            for (int i = 0; i < bits.size(); i++)
            {
                cell_state[i] = bits[bits.size() - i - 1];
            }

            auto& cell = grid.get_cell(i, j);
            cell = cell_state.to_ulong();
            //std::cout << "(" << i << ", " << j<<"): cell = " << (int)cell.get_state() << " " << cell <<std::endl;
        }
    }
}

void fill_grid(Grid& grid)
{
#pragma omp parallel for
    for(int i = 0; i < grid.get_size(); i++)
    {
        auto& cell = grid.get_cell(i);
        cell = 255;

    }
}

void clear_grid(Grid& grid)
{

#pragma omp parallel for
    for(int i = 0; i < grid.get_size(); i++)
    {
        auto& cell = grid.get_cell(i);
        cell = 0;

    }
}

void put_obstacle(Grid& grid, int i, int j, int d)
{
    for (int y = j; y < j+d; y++ )
        for(int x = i; x < i+d; x++)
        {
            {
                auto& n = grid.get_cell(x, y);
                n = 128;
            }
        }
}

void make_channel(Grid& grid, int d)
{
    for (int y = 0; y < d; y++ )
        for(int x = 0; x < grid.get_x_size(); x++)
        {
            {
                auto& n = grid.get_cell(x, y);
                n = 128;
            }
        }

    for (int y = grid.get_y_size() - d; y < grid.get_y_size(); y++ )
        for(int x = 0; x < grid.get_x_size() ; x++)
        {
            {
                auto& n = grid.get_cell(x, y);
                n = 128;
            }
        }

}

void make_hole(Grid& grid, int x, int y, int r)
{

    for(int i = x; i < x+r ; i++)
    {
        for(int j = y; j < y+r; j++)
        {
            auto& n = grid.get_cell(i, j);
            n = 0;
        }
    }
}

void make_bump(Grid& grid, int x, int y, int r, unsigned char state)
{

    for(int i = x; i < x+r ; i++)
    {
        for(int j = y; j < y+r; j++)
        {
            auto& n = grid.get_cell(i, j);
            n = state;
        }
    }
}

void put_node(Grid& grid, int x, int y, unsigned char state)
{
    auto& n = grid.get_cell(x, y);
    n = state;
}

void rectangle(Grid& grid)
{
    //fill_grid(grid);
    clear_grid(grid);
    int cx = grid.get_x_size()/2;
    int cy = grid.get_y_size()/2;

    auto& n = grid.get_cell(cx, cy);
    n = char(63);
    std::cout <<"initial node at pos (" << cx << ", "<< cy << ") = " <<  n << std::endl;
}

void print(const Grid& grid, int i, int j)
{
    std::cout << std::endl;
    for (int y = 0; y < grid.get_y_size(); y++)
    {
        for (int x = 0; x < grid.get_x_size(); x++)
        {
            const auto& c = grid.get_cell(x, y);
            if((i == x) && (j == y))
            {
                std::cout << "("<< (int) c.get_state() << ") ";
            }
            else
                std::cout << " "<< (int) c.get_state() << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl << std::endl;
}


#endif //_FHP_GRID_UTILS_HPP_
