#include <gtest/gtest.h>
#include <CoarseGrid.hpp>
#include <GridUtils.hpp>
#include <FHPDynamics.hpp>
#include <FHPStatistics.hpp>
#include <CoarseGrid.hpp>


TEST (GRID, node_density)
{
    int N = 1<<6;
    int M = 1<<5;
    Grid grid(N, M);

    double rho1 = check_grid_node_density(grid);

    randomize_grid(grid, 4);

    double rho2 = check_mass_conservation(grid);

    std::cout << "rho1 = " << rho1 / (N*M) << "\trho2 = " << rho2 / (N*M)  << std::endl;
}

TEST (GRID, per_node_avg_density)
{
    int N = 1 << 5;
    int M = 1 << 5;


    CoarseGrid grid (N, M, 1<<3, 1 << 3);
    for (int density = 2; density < 7; density++)
    {
        randomize_grid(grid, density);
        std::cout << density << " " << grid.calculate_avg_density() << std::endl;
    }
}

TEST (GRID, randomize_grid_due_density)
{
    //shuffle randomizer
    std::random_device rd;
    std::mt19937 g(rd());

    //normal distribution randomizer
    boost::random::mt19937 rnd;
    rnd.seed(time(0));

    //get normal distribution with mean = density, and sigma = 1
    boost::normal_distribution<> distribution(5,1);
    boost::variate_generator< boost::random::mt19937, boost::normal_distribution<>>
                number_of_particles(rnd, distribution);


    for (int i = 0; i < 100; i++)
    {
        std::bitset<8> b(0);
        std::vector<bool> bits(6);

        int np = (int) number_of_particles();
        std::cout << "n = " << np << "\t";
        for(int i = 0; i < np; i++)
        {
            bits[i] = true;
        }
        std::cout << "init=";
        for(int i = 0; i < bits.size(); i++)
        {
            std::cout << bits[i];
        }
        std::cout << "\t";

        //std::shuffle(&b[0], &b[0] + b.size(), g);

        std::shuffle(bits.begin(), bits.end(), g);

        std::cout << "shuffled=";
        for(int i = 0; i < bits.size(); i++)
        {
            std::cout << bits[i];
        }

        for (int i = 0; i < bits.size(); i++)
        {
            b[i] = bits[bits.size() - i - 1];
        }

        std::cout << "\t" << b << ", " << b.to_ulong() << std::endl;
    }

}

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();

}
