#ifndef _FHP_RENDERER_HPP_
#define _FHP_RENDERER_HPP_

#include <GL/freeglut.h>
#include <algorithm>

#include "CoarseGrid.hpp"
#include "FHPDynamics.hpp"
#include "FHPStatistics.hpp"


static bool abs_compare(double a, double b)
{
    return (std::abs(a) < std::abs(b));
}

class Renderer
{
public:
    Renderer(CoarseGrid& grid) : grid(grid)
    {

        image_data =
            std::vector<GLubyte>
                (
                    4 *
                    this->grid.get_x_size() *
                    this->grid.get_y_size()
                );

        vertical_size = grid.n_tails_y();

        vert_vel_x.resize(vertical_size);
        vert_vel_y.resize(vertical_size);
        vert_rho.resize(vertical_size);

//        int size = grid.n_tails_x()*grid.n_tails_y();
//        V.resize(size);
//        VX.resize(size);
//        VY.resize(size);
    }

    ~Renderer()
    {
        glDeleteTextures(1, &texture_handler);
    }

    void init(int argc, char* argv[])
    {
        instance = this;
        // inicjalizacja biblioteki GLUT
        glutInit (&argc, argv);

        // inicjalizacja bufora ramki
        glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB);

        // rozmiary głównego okna programu
        double ratio = grid.get_x_size() / grid.get_y_size();
        std::cout << "ratio = " << ratio << std::endl;
        int size = 400;
        glutInitWindowSize (ratio*size, size);

        // utworzenie głównego okna programu
        glutCreateWindow ("FHP LGCA");

        texture_handler = createDensityTexture();
    }

    void run()
    {

        // dołączenie funkcji generującej scenę 3D
        glutDisplayFunc (displayWrapper);

        // dołączenie funkcji wywoływanej przy zmianie rozmiaru okna
        glutReshapeFunc (reshapeWrapper);

        glutIdleFunc(idleWrapper);

        // wprowadzenie programu do obsługi pętli komunikatów
        glutMainLoop();

    }

    void update()
    {

        std::fill(vert_vel_x.begin(), vert_vel_x.end(), 0.0);
        std::fill(vert_vel_y.begin(), vert_vel_y.end(), 0.0);
        std::fill(vert_rho.begin(), vert_rho.end(), 0.0);

//        std::fill(V.begin(), V.end(), 0.0);
//        std::fill(VX.begin(), VX.end(), 0.0);
//        std::fill(VY.begin(), VY.end(), 0.0);

#if !defined(NDEBUG)
        double d0t = 0;
        double d4t = 0;
        double d2t = 0;
        double d34t = 0;


        //std::vector<double> vel_y(grid.n_tails_y());
#endif

        double tmax = 100.0;
        for (int t = 0; t < tmax; t++)
        {
            ImposeFlow(grid);
            Collide(grid);
            BounceBack(grid);
            Propagate(grid);

 //           std::cout << "Particles in the system: " << check_mass_conservation(grid) << std::endl;

            auto rho = grid.get_vertical_density(grid.n_tails_x()/2);

            //vertical data
            auto vvx = grid.get_vertical_vx(grid.n_tails_x() / 2);
            auto vvy = grid.get_vertical_vy(grid.n_tails_x() / 2);

            //sum the quantities for calculating avg values per tile
            for (int i = 0; i < vertical_size; i++)
            {
                vert_vel_x[i] += vvx[i];
                vert_vel_y[i] += vvy[i];
                vert_rho[i] += rho[i];
            }

            //whole field;
//            std::vector<double> vm, vx, vy;
//            grid.get_velocity_mag(vx, vy, vm);


//            for (int i = 0; i < vm.size(); i++)
//            {
//                VX[i] += vx[i];
//                VY[i] += vy[i];
//                V[i] += vm[i];
//            }

#if !defined(NDEBUG)
//        auto d0 = grid.get_vertical_density(0);
//        auto d_fourth = grid.get_vertical_density(grid.n_tails_x() / 4);
//        auto d_half = grid.get_vertical_density(grid.n_tails_x() / 2);
//        auto d_tfourth = grid.get_vertical_density(grid.n_tails_x() * 3.0 / 4);

//        d0t += std::accumulate(d0.begin(), d0.end(), 0.0) / d0.size();
//        d4t += std::accumulate(d_fourth.begin(), d_fourth.end(), 0.0) / d_fourth.size();
//        d2t += std::accumulate(d_half.begin(), d_half.end(), 0.0) / d_half.size();
//        d34t += std::accumulate(d_tfourth.begin(), d_tfourth.end(), 0.0) / d_tfourth.size();



#endif
         }

        // calculate avg over time;
        for (int i = 0; i < vertical_size; i++)
        {
            vert_vel_x[i] /= tmax;
            vert_vel_y[i] /= tmax;
            vert_rho[i] /= tmax;
        }

//        for (int i = 0; i < V.size(); i++)
//        {
//            VX[i] = VX[i] / tmax;
//            VY[i] = VY[i] / tmax;
//            V[i]  = V[i]  / tmax;
//        }


#if !defined(NDEBUG)
//        std::cout << "d0 = " << d0t / tmax << std::endl;
//        std::cout << "d_fourth = " << d4t / tmax << std::endl;
//        std::cout << "d_half = " << d2t / tmax << std::endl;
//        std::cout << "d_tfourth = " << d34t / tmax << std::endl;
//        std::cout << std::endl;
        std::cout << std::endl;
#endif

        update_image();

        glutPostRedisplay();
    }

    static void idleWrapper()
    {
        instance->update();
    }

    static void displayWrapper()
    {
        instance->Display();
    }

    static void reshapeWrapper(int width, int height )
    {
        instance->Reshape(width, height);
    }

private:
    CoarseGrid& grid;

    std::vector<GLubyte> image_data;

    GLuint texture_handler;

    static Renderer* instance;

    int vertical_size;
    std::vector<double> vert_vel_x;
    std::vector<double> vert_vel_y;
    std::vector<double> vert_rho;

    std::vector<double> V;
    std::vector<double> VX;
    std::vector<double> VY;

    void Display()
    {
        // kolor tła - zawartość bufora koloru
        glClearColor( 0.0, 0.0, 0.0, 0.0 );

        // czyszczenie bufora koloru
        glClear( GL_COLOR_BUFFER_BIT );

        // setup texture mapping
        glEnable(GL_TEXTURE_2D);

        //glBindTexture(GL_TEXTURE_2D, texture);
        glBindTexture(GL_TEXTURE_2D, texture_handler);

        glBegin(GL_QUADS);
        glTexCoord2f(0.0, 0.0); glVertex2f(-1.0f, -1.0f);
        glTexCoord2f(1.0, 0.0); glVertex2f( 1.0f, -1.0f);
        glTexCoord2f(1.0, 1.0); glVertex2f( 1.0f,  1.0f);
        glTexCoord2f(0.0, 1.0); glVertex2f(-1.0f,  1.0f);
        glEnd();

        glDisable(GL_TEXTURE_2D);


        glLineWidth(2.5);

        {
            double y_size = 2.0;
            double step = 2.0 / grid.n_tails_y();
            //double tile_size = grid.get_y_tile_size();

            double y_begin = -1.0;
            double x_begin = 0.0;

            double cs = 1.0 / std::sqrt(2.0);
            //double size = vert_vel_x.size() * grid.get_x_tile_size() * grid.get_y_tile_size();

            double avg_rho = std::accumulate(vert_rho.begin(), vert_rho.end(), 0.0) / (double) vertical_size;
            double visc = 1.0 / (12 * avg_rho * std::pow((1-avg_rho), 3)) - 0.125;

            //translate to cartesian
            double cos30 = std::cos(M_PI/6.0);
            double sin30 = std::sin(M_PI/6.0);

            for (int i = 0; i < vertical_size; i++)
            {
                glBegin(GL_LINES);

                double vy_begin = (y_begin + i*step) + step * 0.5;
                double vx_begin = 0;//(x_begin + i*step) + step * 0.5;

                double vx = vert_vel_x[i]; /// size;// / sqrt(vert_vel_x[i]*vert_vel_x[i]);
                double vy = vert_vel_y[i]; /// size;// / sqrt(vert_vel_y[i]*vert_vel_y[i]);

                double v_mag = std::sqrt(vx * vx + vy * vy);

                {
                    std::cout << "i = " << i << " v = " << v_mag
                              << " rho = " << vert_rho[i] << " visc = " << visc << std::endl;
                }

                double x = vx * cos30 - vy * sin30;
                double y = vx * sin30 + vy * cos30;

                glVertex3f(vx_begin, vy_begin, 0.0);
                glVertex3f(vx_begin + y, vy_begin + x, 0.0);

                glEnd();
            }
            std::cout << std::endl;
        }

//        {
//            double y_size = 2.0;
//            double x_size = 2.0;
//            double step_y = y_size / grid.n_tails_y();
//            double step_x = x_size / grid.n_tails_x();
//            //double tile_size = grid.get_y_tile_size();

//            double y_begin = -1.0;
//            double x_begin = -1.0;

//            for (int j = 0; j < grid.n_tails_y(); j++)
//            {
//                for( int i = 0; i < grid.n_tails_x(); i++)
//                {
//                    glBegin(GL_LINES);

//                    int index = i + grid.n_tails_x() * j;

//                    double vy_begin = (y_begin + j*step_y) + step_y * 0.5;
//                    double vx_begin = (x_begin + i*step_x) + step_x * 0.5;


//                    double vm = .1;
//                    double vx = VX[index] * vm;// / sqrt(VX[index]*VX[index]);
//                    double vy = VY[index] * vm;// / sqrt(VY[index]*VY[index]);


//                    glVertex3f(vx_begin, vy_begin, 0.0);
//                    glVertex3f(vx_begin + vy , vy_begin + vx, 0.0);

//                    glEnd();

//                }
//            }
//        }

        glutSwapBuffers();
    }

    //zmiana wielkości okna
    void Reshape( int width, int height )
    {
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        glViewport( 0, 0, width, height );
        // generowanie sceny 3D
        //Display();
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

    }


    GLuint createDensityTexture()
    {
        GLuint tex;

        int width = grid.get_x_size();
        int height = grid.get_y_size();
#ifdef NDEBUG
    #pragma omp parallel for
#endif
        for(int j = 0; j < height; ++j)
        {
            for(int i = 0; i < width; ++i)
            {
                size_t index = j*width + i;

                auto n_particles = grid.get_cell(i, j).get_bits().count();

                auto v = 32 * n_particles;
                //auto v = n_particles.get_state();
                image_data[4*index + 0] = v; // R
                image_data[4*index + 1] = v; // G
                image_data[4*index + 2] = v; // B
                image_data[4*index + 3] = 0; // A
            }
        }
        // allocate a texture name
        glGenTextures( 1, &tex );

        // select our current texture
        glBindTexture( GL_TEXTURE_2D, tex );

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);


        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0,
                     GL_RGBA, GL_UNSIGNED_BYTE, image_data.data());

        return tex;
    }

    void update_image()
    {
        int width = grid.get_x_size();
        int height = grid.get_y_size();


#ifdef NDEBUG
    #pragma omp parallel for
#endif
        for(int j = 0; j < height; ++j)
        {
            for(int i = 0;i<width;++i)
            {
                size_t index = j*width + i;

                auto n_particles = grid.get_cell(i, j).get_bits().count();

                auto v = 32 * n_particles;
                image_data[4*index + 0] = v; // R
                image_data[4*index + 1] = v; // G
                image_data[4*index + 2] = v; // B
                image_data[4*index + 3] = 0; // A
            }
        }
        glBindTexture(GL_TEXTURE_2D, texture_handler);    //A texture you have already created storage for
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0,
                        width, height,
                        GL_BGRA, GL_UNSIGNED_BYTE,
                        image_data.data());
    }
};
#endif //RENDERER_HPP_

