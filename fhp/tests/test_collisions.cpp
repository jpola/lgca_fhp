#include <gtest/gtest.h>
#include <CoarseGrid.hpp>
#include <GridUtils.hpp>
#include <FHPDynamics.hpp>
#include <FHPStatistics.hpp>

/* Organizacja wartości w komórce
 *       4   2
 *  8           1
 *      16  32
 */

// Testy mają wykazać czy podczas zderzeń nie są generowane cząstki
// oraz czy zachowany jest pęd układu;

TEST(COLISIONS, binary9)
{
    Grid grid(1,1);

    auto& c = grid.get_cell(0, 0);
    // 8 == cząstka leci z lewa na prawo, 1 = cząstka leci z prawa na lewo;
    // grid ma w sobie random bit który decyduje o wyniku 18 lub 36
    c = 9;

    std::cout << "init: " <<  c
              << "(" << (int)c.get_state() << ")"<< std::endl;

    int m1 = check_mass_conservation(grid);
    double p1 = check_momentum_conservation(grid);

    Collide(grid);

    auto cr = grid.get_cell(0, 0);

    std::cout << "result state: " <<  cr
              << "(" << (int)cr.get_state() << ")"<< std::endl;

    int m2 = check_mass_conservation(grid);
    double p2 = check_momentum_conservation(grid);

    ASSERT_EQ(m1, m2);
    ASSERT_DOUBLE_EQ(p1, p2);
}

TEST(COLISIONS, binary18)
{
    Grid grid(1,1);

    auto& c = grid.get_cell(0, 0);
    // 2 == cząstka leci z prawy górny, 16 = lewy dolny;
    // grid ma w sobie random bit który decyduje o wyniku 9 lub 36
    c = 18;
    std::cout << "init: " <<  c
              << "(" << (int)c.get_state() << ")"<< std::endl;
    int m1 = check_mass_conservation(grid);
    double p1 = check_momentum_conservation(grid);

    Collide(grid);
    auto cr = grid.get_cell(0, 0);
    std::cout << "result state: " <<  cr
              << "(" << (int)cr.get_state() << ")"<< std::endl;

    int m2 = check_mass_conservation(grid);
    double p2 = check_momentum_conservation(grid);

    ASSERT_EQ(m1, m2);
    ASSERT_DOUBLE_EQ(p1, p2);
}

TEST(COLISIONS, binary36)
{
    Grid grid(1,1);

    auto& c = grid.get_cell(0, 0);
    c = 36;
    std::cout << "init: " <<  c
              << "(" << (int)c.get_state() << ")"<< std::endl;

    int m1 = check_mass_conservation(grid);
    double p1 = check_momentum_conservation(grid);

    Collide(grid);
    auto cr = grid.get_cell(0, 0);
    std::cout << "result state: " <<  cr
              << "(" << (int)cr.get_state() << ")"<< std::endl;

    int m2 = check_mass_conservation(grid);
    double p2 = check_momentum_conservation(grid);

    ASSERT_EQ(m1, m2);
    ASSERT_DOUBLE_EQ(p1, p2);
}

TEST(COLISIONS, triple21)
{
    Grid grid(1,1);

    auto& c = grid.get_cell(0, 0);
    c = 21;
    std::cout << "initial state: " << c << std::endl;

    int m1 = check_mass_conservation(grid);
    double p1 = check_momentum_conservation(grid);

    Collide(grid);
    auto cr = grid.get_cell(0, 0);
    std::cout << "result state: " <<  cr << std::endl;

    int m2 = check_mass_conservation(grid);
    double p2 = check_momentum_conservation(grid);

    ASSERT_EQ(m1, m2);
    ASSERT_DOUBLE_EQ(p1, p2);

}

TEST(COLISIONS, triple42)
{
    Grid grid(1,1);

    auto& c = grid.get_cell(0, 0);
    c = 42;
    std::cout << "initial state: " << c << std::endl;

    int m1 = check_mass_conservation(grid);
    double p1 = check_momentum_conservation(grid);

    Collide(grid);
    auto cr = grid.get_cell(0, 0);
    std::cout << "result state: " <<  cr << std::endl;

    int m2 = check_mass_conservation(grid);
    double p2 = check_momentum_conservation(grid);

    ASSERT_EQ(m1, m2);
    ASSERT_DOUBLE_EQ(p1, p2);

}

//Test wszystkich stanów komórek
TEST (COLISIONS, all)
{
    Grid grid(1,1);

    for (int i = 0; i < 64; i++)
    {
        auto& c = grid.get_cell(0, 0);
        c = static_cast<unsigned char> (i);

        int m1 = check_mass_conservation(grid);
        double p1 = check_momentum_conservation(grid);

        Collide(grid);
        auto cr = grid.get_cell(0, 0);

        int m2 = check_mass_conservation(grid);
        double p2 = check_momentum_conservation(grid);

        ASSERT_EQ(m1, m2);
        ASSERT_DOUBLE_EQ(p1, p2);

    }
}

//Test poprawności implementacji lookup table;
TEST (COLISIONS, all_by_lookup)
{
    Grid grid(1,1);

    for (int i = 0; i < 64; i++)
    {
        auto& c = grid.get_cell(0, 0);
        c = static_cast<unsigned char> (i);

        int m1 = check_mass_conservation(grid);
        double p1 = check_momentum_conservation(grid);

        Collide_by_lookup_table(grid);
        auto cr = grid.get_cell(0, 0);

        int m2 = check_mass_conservation(grid);
        double p2 = check_momentum_conservation(grid);

        ASSERT_EQ(m1, m2);
        ASSERT_DOUBLE_EQ(p1, p2);
    }

}

//Test poprawności lookup table na podstawie tego co daje Dieter
TEST (COLISIONS, calc_vs_lookup)
{
    Grid grid_calc(1,1);

    for (int i = 0; i < 64; i++)
    {

        //the same randombits
        Grid grid_lookup = grid_calc;


        auto& cc = grid_calc.get_cell(0, 0); //value from calc grid;
        auto& cl = grid_lookup.get_cell(0, 0); //value from lookup grid;

        cc = static_cast<unsigned char> (i);
        cl = static_cast<unsigned char> (i);


        Collide(grid_calc);
        Collide_by_lookup_table(grid_lookup);

        auto ccr = grid_calc.get_cell(0, 0);
        auto clr = grid_lookup.get_cell(0, 0);


        ASSERT_EQ(clr.get_state(), ccr.get_state());
    }

}




int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();

}
