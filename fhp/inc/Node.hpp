#ifndef _FHP_NODE_HPP_
#define _FHP_NODE_HPP_

#include <vector>
#include <bitset>
#include <algorithm>
#include <climits>
#include <ostream>
#include <cassert>



//directional node, stores the cells componets for given direction
class Node
{
public:
    Node() : _state(0)
    {

    }

    explicit Node(unsigned char state)
    {
        assert(state <= MAX_STATE && state >= 0);
        _state = state;
    }

    explicit Node(int state)
    {
        assert(state <= MAX_STATE && state >= 0);
        _state = state;
    }

    void set_bit(int pos)
    {
        assert(pos < _size && pos >= 0);
        _state |= (1 << (pos));
    }

    //Bit n will be set if x is 1, and cleared if x is 0.
    void set_value(int pos, bool value)
    {
        assert(pos < _size && pos >= 0);
        _state ^= (-value ^ _state) & (1 << pos);
    }

    void unset_bit(int pos)
    {
        assert(pos < _size && pos >= 0);
        _state &= not (1 << (pos));
    }

    bool check_bit(int pos)
    {
        assert(pos < _size && pos >= 0);
        return ((_state) & (1 << (pos)));
    }

    unsigned char get_state()
    {
        return _state;
    }

    const unsigned char get_state() const
    {
        return _state;
    }

    int size() { return _size; }


    const std::bitset<8> get_bits ( ) const {
        return std::bitset<8>(_state);
    }

    friend std::ostream& operator<<(std::ostream& os, const Node& n)
    {
        os << n.get_bits();
        return os;
    }

    Node& operator=(int value)
    {
        assert(value <= (int)MAX_STATE && value >= 0);
        this->_state = value;
        return *this;
    }

    Node& operator=(const Node& other)
    {
        if (this != &other)
        {
            this->_state = other._state;
        }
        return *this;
    }



private:

    unsigned char _state;
    const int _size = 8;
    const unsigned char MAX_STATE = 255;

};

#endif //_FHP_NODE_HPP_
